﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class _oneSignal : MonoBehaviour {

    public string _keyOneSignal;

    void Start()
    {
        OneSignal.StartInit(_keyOneSignal)
          .HandleNotificationOpened(HandleNotificationOpened)
          .EndInit();
    }
    private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {

    }
}
