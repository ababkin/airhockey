﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoogleAnalitics : MonoBehaviour {

    public GoogleAnalyticsV3 GAnalitics;
	public GoogleAnalyticsV3 GAnaliticsAll;

	void Start () {
        GAnalitics.LogScreen(SceneManager.GetActiveScene().name);
        if (GAnaliticsAll != null) GAnaliticsAll.LogScreen(SceneManager.GetActiveScene().name);
	}
	
}
