﻿using UnityEngine;
using System.Collections;

public class WallCreator : MonoBehaviour {

	public float period = 45f;
	public GameObject WallBonus;
	public GameObject Columns;

	public static bool columns = false;

	private int key = 0;
	public float timer = 15f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void FixedUpdate(){
		if (Pause.pause)
			return;
		if (timer == period) {
			GameObject obj;
			Vector3 position = new Vector3(Random.Range(-4,5),0);
			
			obj = (GameObject)Instantiate(WallBonus,position,Quaternion.identity);
			obj.transform.parent = GameObject.Find("hockey").transform;
		}
		if (timer >= 0)
			timer -= Time.deltaTime;
		else
			timer = period;


		if (columns) {
			ColumnOn ();
		} else
			ColumnOff ();
	}

	private void ColumnOn(){
		if (key == 0) {
			Columns.SetActive (true);
			key++;
		}
	}
	private void ColumnOff(){
		Columns.SetActive (false);
		key = 0;
	}
}
