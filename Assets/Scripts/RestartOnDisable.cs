﻿using UnityEngine;
using System.Collections;

public class RestartOnDisable : MonoBehaviour {
	public PauseObjects puck;

	void OnDisable () {
		Pause.pause = false;
		Buttons.stop = false;
		puck.ResetPos (new Vector2 (0, Constants.mid), false);
				Score.ResetScore ();
		
	}
}