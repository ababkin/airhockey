using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	public TextMesh topTextMesh, botTextMesh;
	private static TextMesh topText, botText;
	private static int topScore, botScore;
	public static int top {
		set {
			topText.text = value.ToString ();
			topScore = value;
		}
		get {
			return topScore;
		}
	}

	public static int bot {
		set {
			botText.text = value.ToString ();
			botScore = value;
		}
		get {
			return botScore;
		}
	}

	public static void ResetScore () {
		top = 0;
		bot = 0;
	}

	void Awake ()
	{
		topText = topTextMesh;
		botText = botTextMesh;
		ResetScore ();
	}
}
