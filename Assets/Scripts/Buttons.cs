﻿using UnityEngine;
using System.Collections;

public class Buttons : MonoBehaviour {
	
	Ray ray;
	RaycastHit raycastHit;
	
	public Sprite up, down, lUp, lDown;
	
	private SpriteRenderer sRenderer;
	private Transform prevBtn;
	private bool btn;
	private float scaleText, scaleImg;
	const float DOWN_SIZE = 0.95f;
	
	public static bool stop = false;

	void Start () {
		sRenderer = this.GetComponent<SpriteRenderer> ();
		Transform t = this.transform.FindChild ("img");
		if (t) {
			scaleImg = t.localScale.x;
		}
		t = this.transform.FindChild ("text");
		if (t) {
			scaleText = t.localScale.x;
		}
	}

	public static void DoVibration () {
		if (Settings.vibration) {
			Handheld.Vibrate ();
		}
	}
	
	private void MouseDown ()
	{
		#region Images when down
		Transform t;
		SpriteRenderer sr;
		Color c;
		TextMesh tm;
		switch (this.tag) {
		default:
			if (sRenderer)
			{
				sRenderer.sprite = down;
			}

			t = this.transform.FindChild ("img");
			if (t)
			{
				t.localScale = new Vector3 (scaleImg * DOWN_SIZE, scaleImg * DOWN_SIZE, 1f);
				sr = t.GetComponent<SpriteRenderer>();
				c = sr.color;
				sr.color = new Color(c.r, c.g, c.b, 0.8f);
			}
			t = this.transform.FindChild ("text");
			if (t)
			{
				t.localScale = new Vector3 (scaleText * DOWN_SIZE, scaleText * DOWN_SIZE, 1f);
				tm = t.GetComponent<TextMesh> ();
				c = tm.color;
				tm.color = new Color(c.r, c.g, c.b, 0.8f);
			}
			break;
		}
		#endregion
	}
	
	private void MouseUp () {
//		Debug.Log (this.tag);
		ResetButtons ();
		
		switch (this.tag) {
//		case "Menu_Start1":
//			Settings.vsBot = true;
//			Application.LoadLevel ("SettingsOne");
//			break;

		case "Menu_Start2":
//			Settings.vsBot = false;
//			Application.LoadLevel ("SettingsTwo");
			break;

		case "Menu_Settings":
//			stop = true;
//			this.GetComponent<MyObjects> ().objects[0].SetActive (true);
		break;
		
		case "Menu_Help":
//			stop = true;
//			this.GetComponent<MyObjects> ().objects[0].SetActive (true);
			break;

		case "Settings_Back_Button":
			Application.LoadLevel ("Menu");
			break;

		case "Settings_Forward_Football":
//			Settings.table = Settings.Table.Football;
//			Application.LoadLevel ("Game");
			break;

		case "Settings_Forward_Button":
			Application.LoadLevel ("Game");
			break;

		case "Settings_Forward_Hockey":
			Settings.table = Settings.Table.Hockey;
			Application.LoadLevel ("Game");
			break;
			
		case "Settings_Forward_AirHockey":
			Settings.table = Settings.Table.Cosmos;
			Application.LoadLevel ("Game");
			break;

		case "Settings_Change":
//			transform.GetComponent<Change> ().ChangeValue ();
			break;
			
		case "Game_Pause_Resume":
			Pause.pause = false;
			break;

		case "Game_Pause_Exit":
			Application.LoadLevel ("Menu");
			break;

		case "Game_Pause":
			stop = true;
			Pause.pause = !Pause.pause;
//			ResultScore.savedBot = Score.bot;
//			ResultScore.savedTop = Score.top;
			this.GetComponent<Pause> ().pauseObject.SetActive (true);
			Pause.pause = true;
			break;
			
		case "Menu_VK":
			Application.OpenURL ("https://vk.com/appliciada");
			break;
			
		case "Menu_FB":
			Application.OpenURL ("https://www.facebook.com/groups/appliciada");
			break;

		default:
			break;
		}
	}
	
	private void ResetButtons () {
		#region Images when up
		Transform t;
		SpriteRenderer sr;
		Color c;
		TextMesh tm;
		switch (this.tag) {
		default:
			if (sRenderer) {
				sRenderer.sprite = up;
			}

			t = this.transform.FindChild ("img");
			if (t) {
				t.localScale = new Vector3 (scaleImg, scaleImg, 1f);
				sr = t.GetComponent<SpriteRenderer>();
				c = sr.color;
				sr.color = new Color(c.r, c.g, c.b, 1f);
			}
			t = this.transform.FindChild ("text");
			if (t) {
				t.localScale = new Vector3 (scaleText, scaleText, 1f);
				tm = t.GetComponent<TextMesh> ();
				c = tm.color;
				tm.color = new Color(c.r, c.g, c.b, 1f);
			}
			break;
		}
		#endregion
	}

	void Update () {
		if (stop) return;
		if (Input.GetMouseButtonDown(0) && Input.touchCount < 2) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out raycastHit, 100)) {
				if (raycastHit.transform == transform) {
					prevBtn = raycastHit.transform;
					MouseDown ();
				}
			}
		}
		
		if (Input.GetMouseButtonUp(0) && Input.touchCount < 2 && prevBtn) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out raycastHit, 100) && prevBtn == raycastHit.transform && prevBtn == this.transform) {
				prevBtn = null;
				MouseUp ();
			} else {
				ResetButtons ();
				prevBtn = null;
			}
		}

		if (Input.GetMouseButton (0) && Input.touchCount < 2 && prevBtn) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out raycastHit, 100) && prevBtn == raycastHit.transform && prevBtn == this.transform) {
				MouseDown ();
			} else {
				ResetButtons ();
			}
		}
	}
}

