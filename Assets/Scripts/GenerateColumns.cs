﻿using UnityEngine;
using System.Collections;

public class GenerateColumns : MonoBehaviour {
	//Ray ray;
	RaycastHit2D raycastHit;
	Vector3 position;
	public GameObject Column;
	GameObject obj;

	int onTop = 1; 
	int onRight = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnEnable () {
		int count = 4;
		while (count > 0) {
			switch(count){
			case 4:
				onTop = 1; 
				onRight = 1;
				break;
			case 3:
				onTop = 1; 
				onRight = -1;
				break;
			case 2:
				onTop = -1; 
				onRight = -1;
				break;
			case 1:
				onTop = -1; 
				onRight = 1;
				break;
			}

			position = new Vector3 (Random.Range(2,6)*onRight, Random.Range (2, 4)*onTop, 0);
			raycastHit = Physics2D.Raycast (position, new Vector3 (0, 0, 1));
//			Debug.Log(raycastHit.transform.gameObject.name);
			if(raycastHit.transform.gameObject.name == "top content" || raycastHit.transform.gameObject.name == "bot content"){
				obj = (GameObject)Instantiate(Column,position,Quaternion.identity);
				obj.transform.parent = GameObject.Find("Columns").transform;
				count--;
			}
		}
			
	}

	void OnDisable() {
		Transform[] obj = this.GetComponentsInChildren<Transform>(true);
		for (int i = 0; i < obj.Length; i++) {
			if(obj[i].name == "Column(Clone)")
			//Debug.Log (obj [i]);
			Destroy (obj [i].gameObject);

		}
	}
}
