﻿using UnityEngine;
using System.Collections;

public static class GameData{
	public static string allTextColor;
	public static string textwordSettins;
	public static string particleSystemColor;
	public static string pointsYou;
	public static string pointsApponent;
	public static string textWordYouLose;
	public static double particleAlpha;

	public static bool showPromo;
	public static int countNeedOpenForShowPromo = 2;
	public static string getFullVersionLink;
	public static string ru = "Русский";
	public static string en = "English";
	public static string sp = "Spanish";
	public static string fr = "France";

	public static bool readyFullScreen;
	public static bool readyVideo;
}
