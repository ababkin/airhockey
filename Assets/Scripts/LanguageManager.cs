﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LanguageManager : MonoBehaviour {
	TextMesh mesh;
	Text meshUI;
	public Places place;
	public bool ui;
	public bool changeOnEnable;

	void Start ()
	{
		if (ui) {
			meshUI = this.GetComponent<Text> ();
		} else {
			mesh = this.GetComponent<TextMesh> ();
		}
		ChangeText (Texts.GetText (place));
	}

	void ChangeText (string newText) {
		if (ui) {
			meshUI.text = newText;
		} else {
			mesh.text = newText;
		}
	}

	void OnEnable ()
	{
		if (changeOnEnable)
		{
			ChangeText (Texts.GetText (place));
		}
	}
}

public enum Places
{
	More_Games,
	One_player,
	Two_players,
	Settings,
	Help,
	Difficult,
	Skin,
	Speed,
	Vibration,
	Easy,
	Medium,
	Hard,
	Slow,
	Fast,
	Hockey,
	Football,
	Cosmos,
	On,
	Off,
	Resume,
	Exit,
	Pause,
	YouWin,
	BlueWin,
	RedWin,
	Yes,
	Lost,
	Go,
	MaxScore,
	Start,
	RateUs,
	NoThanks,
	You,
	No,
	Win,
	WinOponent,
	FullVersion,
	Sure
}

public enum Language
{
	English,
	Russian,
	Spanish,
	French
}

public static class Texts
{
	private static string[,] value = {
		{"More","Больше","Mejor","Mieux"},
		{"1 player","1 игрок","1 jugador","1 joueur"},	//One_player
		{"2 players","2 игрока","2 jugadores","2 joueurs"},	//Two_players
		{"Settings","Настройки","Opciones","Ajustage"},	//Settings
		{"Full version","Полная версия","Full version","Full version"},	//Help
		{"Difficulty","Сложность","Dificultad","Difficulté"},	//Difficult
		{"Skin","Скин","Skin","Skin"},	//Skin	
		{"Speed","Скорость","Velocidad","Vitesse"},	//Speed
		{"Vibration","Вибрация","Vibración","Vibration"},	//Vibration
		{"Easy","Легко","Fácil","Facile"},	//Easy
		{"Normal","Средне","Medio","Normalement"},	//Medium
		{"Hard","Тяжело","Difícil","Dificile"},	//Hard
		{"Slow","Медленно","Lento","Lentement"},	//Slow
		{"Fast","Быстро","Rápido","Vite"},	//Fast
		{"Hockey","Хоккей","Hockey","Hockey"},	//Hockey
		{"Football","Футбол","Fútbol","Football"},	//Football
		{"Space","Космос","Cosmos","Cosmos"},	//Cosmos
		{"On","Вкл","On","On"},	//On
		{"Off","Выкл","Off","Off"},	//Off
		{"Continue","Продолжить","Continuar","Continuer"},	//Resume
		{"Exit","Выход","Salida","Sortie"},	//Exit
		{"Pause","Пауза","Pausa","Pause"},	//Pause
		{"You won!","Вы победили!","¡Ha ganado Usted!","Vous êtes triomphé"},	//YouWin
		{"Blue won","Синий победил","El azul ha ganado","Bleu est triomphé"},	//BlueWin
		{"Red won","Красный победил","El rojo ha ganado","Rouge est triomphé"},	//RedWin
		{"Yes","Да","Si","Oui"},	//Yes
		{"You lost","Вы проиграли","Ha perdido Usted","Perdu"},	//Lost
		{"Go!","Начали!","¡Comiencen!","Au début"},	//Go
		{"Max score","Max счет","Puntuaciones","Maximum"},	//MaxScore
		{"Start","Старт","Comienzo","Depart"},	//Start
		{"Rate us!","Оцените нас!","Califiquenos!","Qualifiez nous!"},	//RateUs
		{"No, thanks","Нет, спасибо","No, gracias","Non, merci"},	//NoThanks
		{"You","Вы","Usted","Vous"},	//You
		{"No","Нет","No","Non"},
		{"Win", "Победил", "Ganado", "Triomphé"},
		{"Opponent wins", "Противник победил","Opponet wins", "Opponent wins"},
		{GameData.en, GameData.ru, GameData.sp, GameData.fr},
		{"Are you sure?", "Вы уверены?", "¿Seguro?", "Etes-vous sûr?"}
	};
	
	public static string GetText (Places what)
	{
		return value [(int)what, (int)Settings.language];
	}

	public static void SetText (Places what, Places lang, string text)
	{
		value [(int)what, (int)lang] = text;
	}
}
