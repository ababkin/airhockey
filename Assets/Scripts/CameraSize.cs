﻿using UnityEngine;
using System.Collections;

public class CameraSize : MonoBehaviour {
	public static float resize;
	public bool monoBackground;
	public int standartWidth = 2, standartHeight = 3;
	public GameObject background;

	void Awake () 
	{
		float coef = Screen.height / (float)(Screen.width);
		resize = standartWidth/(float)standartHeight*coef;
		float resize2 = (monoBackground ? 1 : resize);

		if (standartHeight<standartWidth) {
			float temp = resize;
			resize = resize2;
			resize2 = temp;
		}
		//spetial for commit
		if (coef>standartHeight/(float)standartWidth)
		{
			Vector3 vector = background.transform.localScale;
			background.transform.localScale = new Vector3 (vector.x * resize2 , vector.y * resize, vector.z);
		}
		else
		{
			Vector3 vector = background.transform.localScale;
			background.transform.localScale = new Vector3 (vector.x / resize, vector.y / resize2, vector.z);
			resize = 1;
		}

		if (standartHeight<standartWidth)
		{
			float temp = resize;
			resize = resize2;
			resize2 = temp;
		}

		GetComponent<Camera>().orthographicSize *= resize;
	}
}
