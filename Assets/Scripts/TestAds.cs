﻿using AppodealAds.Unity.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAds : MonoBehaviour {
    public string AppodealKey;
    // Use this for initialization
    void Start () {
        /*Appodeal.setInterstitialCallbacks(this);
        Appodeal.setNonSkippableVideoCallbacks(this);
        Appodeal.setRewardedVideoCallbacks(this);*/
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(AppodealKey, Appodeal.BANNER_BOTTOM | Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO);        
    }

    public void ShowInter()
    {
        Appodeal.show(Appodeal.INTERSTITIAL);
    }

    public void ShowBaner()
    {
        Appodeal.show(Appodeal.BANNER_BOTTOM);
    }

    public void ShowRevard()
    {
        Appodeal.show(Appodeal.REWARDED_VIDEO);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
