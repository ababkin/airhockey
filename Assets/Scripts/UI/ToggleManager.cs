﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleManager : MonoBehaviour {
	public Button[] buttons;
	public Sprite up, down;
	SpriteState stateUp, stateDown;

	void Awake () {
		stateUp = new SpriteState ();
		stateUp.highlightedSprite = up;
		stateUp.pressedSprite = down;
		
		stateDown = new SpriteState ();
		stateDown.highlightedSprite = down;
		stateDown.pressedSprite = down;
		int index = 1;
		switch (tag) {
		case "Difficult":
			index = (int)Settings.difficult+1;
			break;
		case "Table":
			index = (int)Settings.table+1;
			break;
		case "Speed":
			index = (int)Settings.speed+1;
			break;
		case "Vibro":
			index = Settings.vibration?1:2;
			break;
		}
		SetGraph (index);
	}

	public void SetGraph (int index) {
		for (int i = 0; i < buttons.Length; i++) {
			if (i+1==index) {
				buttons[i].image.sprite = down;
				buttons[i].spriteState = stateDown;
			} else {
				buttons[i].image.sprite = up;
				buttons[i].spriteState = stateUp;
			}
		}
	}
}
