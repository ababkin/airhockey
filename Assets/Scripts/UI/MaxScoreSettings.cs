﻿using UnityEngine;
using UnityEngine.UI;

public class MaxScoreSettings : MonoBehaviour {
	Text score;
	void Awake () {
		score = this.GetComponent<Text> ();
		score.text = Settings.maxScore.ToString ();
	}
	public void IncScore () {
		Settings.maxScore++;
		score.text = Settings.maxScore.ToString ();
	}
	public void DecScore () {
		Settings.maxScore--;
		score.text = Settings.maxScore.ToString ();
	}
}
