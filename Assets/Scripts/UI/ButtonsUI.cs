using UnityEngine;
using System.Collections;

public class ButtonsUI : MonoBehaviour {

	public void SetDifficult (int val) {
		Settings.difficult = (Settings.Difficult)val;
	}
	public void SetTable (int val) {
		Settings.table = (Settings.Table)val;
	}
	public void SetSpeed (int val) {
		Settings.speed = (Settings.Speed)val;
	}
	public void SetVibro (bool val) {
		Settings.vibration = val;
	}
	public void TurnButtons (bool val) {
		Buttons.stop = !val;
	}
	public void Load_Level (string level) {
		if (level.Length>0) {
			if (level.Equals ("Quit")) {

				Application.Quit ();
			} else {
				Application.LoadLevel (level);
			}
		}
	}


	void OnApplicationQuit() {
//		if (GameData.readyVideo) {
//			Debug.Log("video " + GameData.readyVideo);
//			AdSDK.ShowVideo ();
//		} else {
//			AdSDK.ShowFullscreen();
//		}
	}

	public void TurnPause (bool val) {
		Pause.pause = val;
	}

	public void Start1() {
		Settings.vsBot = true;

		Application.LoadLevel ("SettingsOne");
	}

	public void Start2() {
		Settings.table = Settings.Table.Cosmos;
		Settings.vsBot = false;
		Application.LoadLevel ("Game");
	}

	public void Help() {
		Application.OpenURL (GameData.getFullVersionLink);
	}

	public void SettingForwardButton() {
		Application.LoadLevel ("Game");
	}

	public void ChangeValue() {
		transform.GetComponent<Change> ().ChangeValue ();
	}

	public void GamePausa() {
		Pause.pause = true;
		this.GetComponent<Pause> ().pauseObject.SetActive (true);
	}

	public void More(){
		Application.OpenURL ("http://admobapp.com");
	}


}