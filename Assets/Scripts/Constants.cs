﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour {
	public const float mid = 0, height = 4.13f, width = 3f;
	public const float sizeHelpStr = 0.25f;
	public const float yStartTop = 6;
	public const float yStartBot = -6;
	public const float yPuckTop = 1.6f;
	public const float yPuckBot = -1.6f;
	public static float[] PUCK_SPEEDS = new float[3] {30f, 55f, 70f};
}
