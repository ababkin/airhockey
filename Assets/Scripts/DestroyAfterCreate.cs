﻿using UnityEngine;
using System.Collections;

public class DestroyAfterCreate : MonoBehaviour {
	public float delay = 0.4f;
//	Color[] colors = new Color[3]
//	{
//		new Color (1, 1, 1, 180/255f),//Hockey
//		new Color (100/255f, 100/255f, 100/255f, 180/255f),//Football
//		new Color (161/255f, 157/255f, 20/255f, 180/255f)//Cosmoc
//	};

	void Awake () {
		this.GetComponent<ParticleSystem> ().startColor = HexToColor (GameData.particleSystemColor, GameData.particleAlpha);
	}

	void Start () {
		Invoke ("DestroyDelay", delay);
	}
	
	void DestroyDelay () {
		Destroy (this.gameObject);
	}

	Color HexToColor(string hex, double colorAlpha) {
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, (byte)(colorAlpha * 255 % 256));
	}
}
