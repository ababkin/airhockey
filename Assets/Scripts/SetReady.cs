﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetReady : MonoBehaviour {
	public Text text;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		text.text = GameData.readyVideo.ToString () + "; " + GameData.readyFullScreen.ToString ();
	}
}
