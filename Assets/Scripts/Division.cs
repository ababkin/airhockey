﻿using UnityEngine;
using System.Collections;

public class Division : MonoBehaviour {
	// Use this for initialization
	public GameObject Puck;
	public static bool enable = false;
	public static float lifeTime = 0;
	public static bool goal = false;
	private GameObject obj;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (enable == true)
			Create ();
		enable = false;
		if (goal) 
			Destroy (obj);
		goal = false;
	}
	public void Create()
	{
		obj = (GameObject)Instantiate(Puck,Puck.transform.position,Quaternion.identity);
		obj.GetComponent<Rigidbody2D>().velocity = new Vector2 (Puck.GetComponent<Rigidbody2D>().velocity.x*-1f, Puck.GetComponent<Rigidbody2D>().velocity.y);
		obj.transform.parent = GameObject.Find("hockey").transform;
		Destroy (obj, lifeTime);
	}
}
