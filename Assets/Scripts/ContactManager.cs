﻿using UnityEngine;
using System.Collections;

public class ContactManager : MonoBehaviour {
	public GameObject particle;
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.transform.tag.Equals ("Player")) {
			foreach (ContactPoint2D point in coll.contacts) {
				GameObject newPartic = Instantiate (particle) as GameObject;
				newPartic.transform.position = new Vector3 (point.point.x, point.point.y, -5);
			}
			Buttons.DoVibration ();
		}
	}
}
