﻿using UnityEngine;
using System.Collections;

public class MagnetPower : MonoBehaviour {
	public static GameObject Club;
	public float radius = 3f;
	public float power = 10f;
	public static bool enable = false;
	public static int bonusType = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		if (enable) {
			switch (bonusType){
			case 0:
				return;
			case 1: 
				Vector3 club = Club.transform.position;
				Vector3 direction = this.transform.position - club;
				if (Vector3.Distance (this.transform.position, club) < radius) {
					float per = 1 - (direction.magnitude / radius);
					this.GetComponent<Rigidbody2D>().AddForce (direction.normalized * per * power * -1);
				}
				break;
			}
		}
	}
}
