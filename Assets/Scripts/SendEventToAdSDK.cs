﻿using UnityEngine;
using System.Collections;

public class SendEventToAdSDK : MonoBehaviour {
	
	public string GetPackageNaneFromURL(string URL) {
		if (URL.Length > 0) {
			int posSubstringStart = URL.IndexOf ("id=") + 3;
			if( posSubstringStart >= URL.Length) {
				string [] split = (URL.Substring(posSubstringStart)).Split(new char [] {'&'});
				return split[0];
			}
		}
		return "";
	}
}
