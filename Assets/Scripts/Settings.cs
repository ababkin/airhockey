﻿using UnityEngine;
using System.Collections;

public static class Settings{
	public enum Difficult
	{
		Easy,
		Medium,
		Hard
	}
	public enum Table
	{
		Hockey,
		Football,
		Cosmos	
	}
	public enum Speed
	{
		Slow,
		Medium,
		Fast
	}

	public static bool vibration {
		get {
			return PlayerPrefs.GetInt ("Vibration") == 1;
		}
		set {
			PlayerPrefs.SetInt ("Vibration", value?0:1);
		}
	}
	public static bool vsBot;
	public static Language language {
		get {
			switch (Application.systemLanguage) {
			case SystemLanguage.Russian:
				return Language.Russian;

			case SystemLanguage.Spanish:
				return Language.Spanish;

			case SystemLanguage.French:
				return Language.French;

			default:
				return Language.English;
			}
		}
		private set {}
	}
	const int MAX_SCORE = 15, MIN_SCORE = 3;
	public static int maxScore {
		get {
			int score = PlayerPrefs.GetInt ("MaxScore");
			if (score == 0) {
				score = 3;
				PlayerPrefs.SetInt ("MaxScore", score);
			}
			return score;
		}
		set {
			if (value >= MIN_SCORE && value <= MAX_SCORE) {
				PlayerPrefs.SetInt ("MaxScore", value);
			}
		}
	}
	public static Difficult difficult {
		get {
			return (Difficult)PlayerPrefs.GetInt ("Difficult");
		}
		set {
			PlayerPrefs.SetInt ("Difficult", (int)value);
		}
	}
	public static Speed speed {
		get {
			return (Speed)PlayerPrefs.GetInt ("Speed");
		}
		set {
			PlayerPrefs.SetInt ("Speed", (int)value);
		}
	}
	public static Table table {
		get {
			return (Table)PlayerPrefs.GetInt ("Table");
		}
		set {
			PlayerPrefs.SetInt ("Table", (int)value);
		}
	}
}