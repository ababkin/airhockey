﻿//using http://wiki.unity3d.com/index.php?title=HexConverter
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum ColorForText {
	AllTextColor,
	SettingWordColor,
	ParticleColor,
	PointsYou,
	PointsApponent,
	YouLose

}
public class ChangeColotForText : MonoBehaviour {
	public bool ui;
	public ColorForText colorFor;
	Color color;
	// Use this for initialization
	void Start () {

		//зфкешсдуЫныеуь
		if(ui) {
			color = GetComponent<Text>().color;
			ChangeTextForUi();
		} else {
			color = GetComponent<TextMesh>().color;
			ChangeTextForTextMesh();
		}
	}
	
	Color HexToColor(string hex) {
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, (byte)(color.a * 255 % 256));
	}

	private void ChangeTextForUi() {
		switch(colorFor) {
			case ColorForText.AllTextColor: 
				GetComponent<Text>().color = HexToColor (GameData.allTextColor); 
				break;
			case ColorForText.SettingWordColor:
				GetComponent<Text>().color = HexToColor (GameData.textwordSettins); 
				break;
			case ColorForText.PointsYou:
				GetComponent<Text>().color = HexToColor (GameData.pointsYou); 
				break;
			case ColorForText.PointsApponent:
				GetComponent<Text>().color = HexToColor (GameData.pointsApponent); 
				break;
			case ColorForText.YouLose:
				GetComponent<Text>().color = HexToColor (GameData.textWordYouLose); 
				break;
			default: 
				break;
		}
	}

	private void ChangeTextForTextMesh() {
		switch(colorFor) {
		case ColorForText.AllTextColor: 
			GetComponent<TextMesh>().color = HexToColor (GameData.allTextColor); 
			break;
		case ColorForText.SettingWordColor:
			GetComponent<TextMesh>().color = HexToColor (GameData.textwordSettins); 
			break;
		case ColorForText.PointsYou:
			GetComponent<TextMesh>().color = HexToColor (GameData.pointsYou); 
			break;
		case ColorForText.PointsApponent:
			GetComponent<TextMesh>().color = HexToColor (GameData.pointsApponent); 
			break;
		case ColorForText.YouLose:
			GetComponent<TextMesh>().color = HexToColor (GameData.textWordYouLose); 
			break;
		default: 
			break;
		}
	}
}
