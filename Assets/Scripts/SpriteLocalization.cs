﻿using UnityEngine;
using UnityEngine.UI;

public class SpriteLocalization : MonoBehaviour {
	public Sprite[] sprites;
	public Sprite[] downSprites;
	public bool ui = true;
	void Awake () {
		if (ui) {
			this.GetComponent<Image> ().sprite = sprites [(int)Settings.language];
			if (downSprites.Length>0) {
				Button b = this.GetComponent<Button> ();
				SpriteState myState = new SpriteState ();
				myState.highlightedSprite = sprites [(int)Settings.language];
				myState.pressedSprite = downSprites [(int)Settings.language];
				b.spriteState = myState;
			}
		} else {
			this.GetComponent<SpriteRenderer> ().sprite = sprites [(int)Settings.language];
			if (downSprites.Length>0) {
				Buttons b = this.GetComponent<Buttons> ();
				b.up = sprites [(int)Settings.language];
				b.down = downSprites [(int)Settings.language];
			}
		}
	}
}
