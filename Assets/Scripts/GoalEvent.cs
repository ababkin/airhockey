﻿using UnityEngine;
using System.Collections;

public class GoalEvent : MonoBehaviour {
	public bool top;
	public PauseObjects puck, topClub, botClub;
	public GameObject goal, topWin, botWin, youLose, youWin;
	void OnCollisionEnter2D(Collision2D coll) {
		if(coll.gameObject.name == "puck"){		
			Pause.pause = true;
			if (top) {
				//goal to top
	//			Debug.Log ("goal to top");
				Score.bot++;
				if (Score.bot == Settings.maxScore) {
					//bot win
	//				ResultScore.savedBot = Score.bot;
	//				ResultScore.savedTop = Score.top;
	//				Score.ResetScore ();
					Puck.start = true;
					puck.ResetPos (Vector2.zero, false);
					topClub.ResetPos (new Vector2 (0, Constants.yStartTop), false);
					botClub.ResetPos (new Vector2 (0, Constants.yStartBot), false);
					if (Settings.vsBot) {
						youWin.SetActive (true);
					} else {
						botWin.SetActive (true);
					}
					RateUsCaller.count++;
					Buttons.stop = true;
				} else {
					//just +1 for bot
	//				ResultScore.savedBot = Score.bot;
	//				ResultScore.savedTop = Score.top;
					Buttons.stop = true;
					Puck.start = true;
					puck.ResetPos (new Vector2 (0, Constants.yPuckTop), true);
					topClub.ResetPos (new Vector2 (0, Constants.yStartTop), true);
					botClub.ResetPos (new Vector2 (0, Constants.yStartBot), true);
					goal.SetActive (true);
					Puck.time = 0;
				}
			} else {
				//goal to bottom
	//			Debug.Log ("goal to bottom");
				Score.top++;
				if (Score.top == Settings.maxScore) {
					//top win
	//				ResultScore.savedBot = Score.bot;
	//				ResultScore.savedTop = Score.top;
	//				Score.ResetScore ();
					Puck.start = true;
					puck.ResetPos (Vector2.zero, false);
					topClub.ResetPos (new Vector2 (0, Constants.yStartTop), false);
					botClub.ResetPos (new Vector2 (0, Constants.yStartBot), false);
					if (Settings.vsBot) {
						youLose.SetActive (true);
					} else {
						topWin.SetActive (true);
					}
					RateUsCaller.count++;
					Buttons.stop = true;
				} else {
					//just +1 for top
					ResultScore.savedBot = Score.bot;
					ResultScore.savedTop = Score.top;
	//				Buttons.stop = true;
					Puck.start = true;
					puck.ResetPos (new Vector2 (0, Constants.yPuckBot), true);
					topClub.ResetPos (new Vector2 (0, Constants.yStartTop), true);
					botClub.ResetPos (new Vector2 (0, Constants.yStartBot), true);
					goal.SetActive (true);
					Puck.time = 0;
				}
			}
			//Debug.Log("GOAL");
				Division.goal = true;
		}
	}
}
