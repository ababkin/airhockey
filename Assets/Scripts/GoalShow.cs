﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GoalShow : MonoBehaviour {
	public static bool isActive;
	public GameObject windowPause;

	void OnEnable () {
		isActive = true;
		GetComponent<Animation>().Play ();
	}
	
	void OnDisable () {
		Pause.pause = false;
		Buttons.stop = false;
	}
	
	void Update(){
		if (windowPause.gameObject.activeSelf ) {
			foreach (AnimationState state in GetComponent<Animation>()) {
				state.speed = 0F;
			}
		} else {
			foreach (AnimationState state in GetComponent<Animation>()) {
				state.speed = 1;
			}
		}
		if (!windowPause.gameObject.activeSelf && !GetComponent<Animation>().isPlaying) {
			isActive = false;
			this.gameObject.SetActive (false);
		}
	}
}