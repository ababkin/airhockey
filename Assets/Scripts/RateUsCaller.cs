﻿using UnityEngine;
using System.Collections;

public class RateUsCaller : MonoBehaviour {
	bool show {
		get {
			return PlayerPrefs.GetInt ("RateUs") == 0;
		}
		set {
			PlayerPrefs.SetInt ("RateUs", value?0:1);
		}
	}
	public static int count = 0;
	public GameObject rateUs;
	void Start () {
		if (count>2 && show) {
			rateUs.SetActive (true);
			count = 0;
		}
	}
	public void CallRateUs () {
		show = false;
		//TODO Rate us call
		Application.OpenURL ("https://play.google.com/store/apps/details?id=free.app.airhockey");
	}
}
