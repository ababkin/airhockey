﻿using UnityEngine;
using System.Collections;

public class BonusInstantiate : MonoBehaviour {
	public GameObject[] bonusList;
	public float period = 18f;
	private float timer;
	private bool top = false;

	public GameObject[] DefGoal;
	public GameObject[] BigGoal;
	public static bool isEnableGoal = false;
	public static int topGoal = -1;



	// Use this for initialization
	void Start () {
		timer = 10f;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Pause.pause)
			return;
		if (timer == period) {
			GameObject obj;
			int index = Random.Range(0,bonusList.Length);
			Vector3 position = Vector3.zero;
			if(top){
				top = false;
				position.y = 3f;
				if(Random.Range(0,2) == 0)
					position.x = -3f;
				else
					position.x = 3f;
			}
			else{
				top = true;
				position.y = -3f;
				if(Random.Range(0,2) == 0)
					position.x = -3f;
				else
					position.x = 3f;
			}

			obj = (GameObject)Instantiate(bonusList[index],position,Quaternion.identity);
			obj.transform.parent = GameObject.Find("hockey").transform;
		}
		if (timer >= 0)
			timer -= Time.deltaTime;
		else
			timer = period;

		if (isEnableGoal)
			GoalBig ();
		else
			Default ();


	}
	private void GoalBig(){
		if (topGoal != -1) {
			DefGoal [topGoal].SetActive (false);
			BigGoal [topGoal].SetActive (true);
			topGoal = -1;
		}
	}
	private void Default(){
		for (int i = 0; i < DefGoal.Length; i++) {
			DefGoal [i].SetActive (true);
			BigGoal [i].SetActive(false);
		}
	}


}
