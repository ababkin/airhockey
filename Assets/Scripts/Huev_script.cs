﻿using UnityEngine;
using System.Collections;

public class Huev_script : MonoBehaviour {
	private SpriteRenderer spriteRenderer;
	private CircleCollider2D boxCollider;
	public bool isAwake = true;
	public float width;
	public float height;
	public float radiusForCircle;
	
	float heightSprite;
	float widthSprite;
	
	float scaleY;
	float scaleX;
	
	void Awake () {
		if(isAwake) {
			SetScale ();
		}
	}
	
	public void SetScale() {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		Sprite sprite = spriteRenderer.sprite;
		if (spriteRenderer != null) {
			widthSprite = sprite.texture.width;
			heightSprite = sprite.texture.height;
			scaleX = width*transform.localScale.x / widthSprite;
			scaleY = height*transform.localScale.y / heightSprite;
			
			transform.localScale = new Vector3 (scaleX, scaleY, 1);
			
			boxCollider = GetComponent<CircleCollider2D> ();
			if(boxCollider != null) {
				boxCollider.radius = widthSprite/100f/2f;	
			}
		}
	}
}
