﻿using UnityEngine;
using System.Collections;

public class CoverMouse : MonoBehaviour {
	public GameObject content;
	private Vector2 position;
	private Vector2 center;
	private Vector2 size;
	public bool top;
	public static float radius;

#if UNITY_EDITOR
	private bool down;
#else
	private int myTouch;
#endif

	public void SetPosition (Vector2 vector) {
		position = vector;
	}
	
	void Start () {
#if UNITY_EDITOR
		down = false;
#else
		myTouch = -1;
#endif
		center = new Vector2 (content.transform.position.x, content.transform.position.y);
		BoxCollider2D box = content.GetComponent<BoxCollider2D> ();
		size = box.size;
		position = this.GetComponent<Rigidbody2D>().position;
		radius = this.GetComponent<Huev_script> ().radiusForCircle;
		if (Settings.vsBot && top) {
			enabled = false;
		}
	}
	void Update () {
#if UNITY_EDITOR
		///
		//radius = this.GetComponent<CircleCollider2D> ().radius*this.transform.localScale.x;
		///
		if (Input.GetMouseButtonUp (0)) {
			down = false;
		}
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray)) {
				return;
			}
			Vector2 origin = new Vector2 (ray.origin.x, ray.origin.y);
			Vector2 direction = new Vector2 (ray.direction.x, ray.direction.y);
			RaycastHit2D[] hits = Physics2D.RaycastAll (origin, direction);
			foreach (RaycastHit2D hit in hits) {
				if (hit.collider != null && hit.transform.gameObject == content) {
					down = true;
					break;
				}
			}
		}
		if (down) {
			if (Pause.pause) return;
			position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		}
#else
		foreach (Touch touch in Input.touches) {
			switch (touch.phase) {
			case TouchPhase.Began:
				if (myTouch == -1) {
					Ray ray = Camera.main.ScreenPointToRay (touch.position);
					if (Physics.Raycast (ray)) {
						return;
					}
					Vector2 origin = new Vector2 (ray.origin.x, ray.origin.y);
					Vector2 direction = new Vector2 (ray.direction.x, ray.direction.y);
					RaycastHit2D[] hits = Physics2D.RaycastAll (origin, direction);
					foreach (RaycastHit2D hit in hits) {
						if (hit.collider != null && hit.transform.gameObject == content) {
							myTouch = touch.fingerId;
							break;
						}
					}
				}
				break;
			case TouchPhase.Ended:
				if (touch.fingerId == myTouch) {
					myTouch = -1;
				}
				break;
			default:
				if (Pause.pause) return;
				if (touch.fingerId == myTouch) {
					position = Camera.main.ScreenToWorldPoint (touch.position);
				}
				break;
			}
		}
#endif
		if (Pause.pause) return;
		//if (position.x > center.x + size.x / 2f - radius) position.x = center.x + size.x / 2f - radius;
		//if (position.x < center.x - size.x / 2f + radius) position.x = center.x - size.x / 2f + radius;
		if (position.y > center.y + size.y / 2f - radius) position.y = center.y + size.y / 2f - radius;
		if (position.y < center.y - size.y / 2f + radius) position.y = center.y - size.y / 2f + radius;
	}

	void FixedUpdate () {
		if (Pause.pause) return;
		this.GetComponent<Rigidbody2D>().MovePosition (position);
	}
}