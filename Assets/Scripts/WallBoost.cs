﻿using UnityEngine;
using System.Collections;

public class WallBoost : MonoBehaviour {

	private bool animOn = false;
	private bool onTable = true;
	public float bonusTime = 15f;
	public float bonusLifeTime = 10f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}
	void FixedUpdate(){
		if (Pause.pause)
			return;
		if (onTable) {
			if (bonusLifeTime > 0)
				bonusLifeTime -= Time.deltaTime;
			else
				Destroy (this.gameObject);
			if (bonusLifeTime <= 3 && animOn == false) {
				GetComponent<Animation>().Play("bonusDestroy");
				animOn = true;
			}
		}
	}
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.name == "puck")
		{
			WallCreator.columns = true;
			Invoke ("Return", bonusTime);
			if (GetComponent<Animation>().Play ())
				GetComponent<Animation>().Stop ();
			onTable = false;
			
			this.gameObject.GetComponent<Renderer>().enabled = false;
			this.gameObject.GetComponent<Collider2D>().enabled = false;
			animOn = true;

		}
	}

	void Return(){
		Debug.Log ("off");
		WallCreator.columns = false;
		Destroy (this.gameObject);
	}
}
