using UnityEngine;
using System.Collections;

public class Change : MonoBehaviour {
	public enum TypeChange
	{
		Difficult,
		Skin,
		Speed
	};

	public enum TypeButton
	{
		Left,
		Right,
		One
	};

	public TypeChange type = TypeChange.Difficult;
	public TypeButton typeButton = TypeButton.One;
	public TextMesh txtMesh = null;

	void Start ()
	{
		UpdateText ();
	}

	void UpdateText ()
	{
		switch (type)
		{
		case TypeChange.Difficult:
			if (txtMesh != null) 
			{
				switch (Settings.difficult)
				{
				case Settings.Difficult.Easy:
					txtMesh.text = Texts.GetText (Places.Easy);
					break;
				case Settings.Difficult.Medium:
					txtMesh.text = Texts.GetText (Places.Medium);
					break;
				case Settings.Difficult.Hard:
					txtMesh.text = Texts.GetText (Places.Hard);
					break;
				}
			}
			break;
			
		case TypeChange.Skin:
			if (txtMesh != null) 
			{
				switch (Settings.table)
				{
				case Settings.Table.Cosmos:
					txtMesh.text = Texts.GetText (Places.Cosmos);
					break;
				case Settings.Table.Football:
					txtMesh.text = Texts.GetText (Places.Football);
					break;
				case Settings.Table.Hockey:
					txtMesh.text = Texts.GetText (Places.Hockey);
					break;
				}
			}
			break;
			
		case TypeChange.Speed:
			if (txtMesh != null)
			{
				switch (Settings.speed)
				{
				case Settings.Speed.Slow:
					txtMesh.text = Texts.GetText (Places.Slow);
					break;
				case Settings.Speed.Medium:
					txtMesh.text = Texts.GetText (Places.Medium);
					break;
				case Settings.Speed.Fast:
					txtMesh.text = Texts.GetText (Places.Fast);
					break;
				}
			}
			break;
		}
	}

	public void ChangeValue ()
	{
		switch (typeButton)
		{
		case TypeButton.Left:
			ChangeLeft ();
			break;

		case TypeButton.Right:
			ChangeRight ();
			break;
		}
	}

	void ChangeRight ()
	{
		switch (type)
		{
		case TypeChange.Difficult:
			if (Settings.difficult == Settings.Difficult.Medium)
			{
				Settings.difficult = Settings.Difficult.Hard;
				break;
			}
			if (Settings.difficult == Settings.Difficult.Easy)
			{
				Settings.difficult = Settings.Difficult.Medium;
				break;
			}
			if (Settings.difficult == Settings.Difficult.Hard)
			{
				Settings.difficult = Settings.Difficult.Easy;
				break;
			}
			break;

		case TypeChange.Skin:
			if (Settings.table == Settings.Table.Football)
			{
				Settings.table = Settings.Table.Hockey;
				break;
			}
			if (Settings.table == Settings.Table.Cosmos)
			{
				Settings.table = Settings.Table.Football;
				break;
			}
			if (Settings.table == Settings.Table.Hockey)
			{
				Settings.table = Settings.Table.Cosmos;
				break;
			}
			break;

		case TypeChange.Speed:
			if (Settings.speed == Settings.Speed.Fast)
			{
				Settings.speed = Settings.Speed.Slow;
				break;
			}
			if (Settings.speed == Settings.Speed.Medium)
			{
				Settings.speed = Settings.Speed.Fast;
				break;
			}
			if (Settings.speed == Settings.Speed.Slow)
			{
				Settings.speed = Settings.Speed.Medium;
				break;
			}
			break;
		}

		UpdateText ();
	}

	void ChangeLeft ()
	{
		switch (type)
		{
		case TypeChange.Difficult:
			if (Settings.difficult == Settings.Difficult.Medium)
			{
				Settings.difficult = Settings.Difficult.Easy;
				break;
			}
			if (Settings.difficult == Settings.Difficult.Hard)
			{
				Settings.difficult = Settings.Difficult.Medium;
				break;
			}
			if (Settings.difficult == Settings.Difficult.Easy)
			{
				Settings.difficult = Settings.Difficult.Hard;
				break;
			}
			break;
		case TypeChange.Skin:
			if (Settings.table == Settings.Table.Football)
			{
				Settings.table = Settings.Table.Cosmos;
				break;
			}
			if (Settings.table == Settings.Table.Hockey)
			{
				Settings.table = Settings.Table.Football;
				break;
			}
			if (Settings.table == Settings.Table.Cosmos)
			{
				Settings.table = Settings.Table.Hockey;
				break;
			}
			break;

		case TypeChange.Speed:
			if (Settings.speed == Settings.Speed.Slow)
			{
				Settings.speed = Settings.Speed.Fast;
				break;
			}
			if (Settings.speed == Settings.Speed.Medium)
			{
				Settings.speed = Settings.Speed.Slow;
				break;
			}
			if (Settings.speed == Settings.Speed.Fast)
			{
				Settings.speed = Settings.Speed.Medium;
				break;
			}
			break;
		}
		UpdateText ();
	}
}
