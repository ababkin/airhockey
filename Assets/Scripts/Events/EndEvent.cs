﻿using UnityEngine;
using System.Collections;

public class EndEvent : MonoBehaviour {
	public static EndEvent inst = null;
	public GameObject popapWin, popapLose;
	void Awake () {
		inst = this;
	}
	public void WinEvent () {
		popapWin.SetActive (true);
	}
	public void LoseEvent () {
		popapLose.SetActive (true);
	}
}
