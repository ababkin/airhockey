﻿using UnityEngine;
using System.Collections;

public class PauseChangeBackground : MonoBehaviour {

	public GameObject objectBackground;
	public Sprite[] newBackground;
	public GameObject[] hideObjects;
	public GameObject[] showObjects;
	void OnEnable ()
	{
		objectBackground.GetComponent<SpriteRenderer> ().sprite = newBackground[(int)Settings.table];
		for (int i = 0; i < hideObjects.Length; i++)
		{
			hideObjects[i].SetActive (false);
		}
		for (int i = 0; i < showObjects.Length; i++)
		{
			showObjects[i].SetActive (true);
		}
	}
}
