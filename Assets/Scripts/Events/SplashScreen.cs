﻿using UnityEngine;
using System.Collections;
//using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

    public MyAppodeal ads;

    void Start() {
#if UNITY_EDITOR
        SceneManager.LoadScene("Menu");

#elif UNITY_ANDROID && !UNITY_EDITOR
        ClosedLoad();
#endif
    }

    void ClosedLoad()
    {
        if (Application.internetReachability.ToString() != "NotReachable")
        {
            ads.ShowADS("ShowInterstitital");
        }
        else if (Application.internetReachability.ToString() == "NotReachable")
        {
            SceneManager.LoadScene("Menu");
        }
    }
}