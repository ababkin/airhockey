﻿using UnityEngine;
using System.Collections;

public class CoverMouseBot : MonoBehaviour
{
	public Transform puck;
	public GameObject content;
	public bool top;
	private Vector2 position;
	private Vector2 center;
	private Vector2 size;
	const float SLOW = 4f, MEDIUM = 8f, FAST = 12f;
	private float radius;
	private float[] diffSpeed;
	private float counter = 0;
	private float timeIn, timeOut;
	private bool lastZone = false;
	
	public void SetPosition (Vector2 vector)
	{
		position = vector;
	}
	
	void Start ()
	{
		diffSpeed = new float[] {SLOW, MEDIUM, FAST};
		position = this.GetComponent<Rigidbody2D>().position;
		center = new Vector2 (content.transform.position.x, content.transform.position.y);
		BoxCollider2D box = content.GetComponent<BoxCollider2D> ();
		size = box.size;
		radius = this.GetComponent<CircleCollider2D> ().radius;
		//Debug.Log (size);
		if (!Settings.vsBot) {
			enabled = false;
		}
		timeIn = 0;
		timeOut = 0;
		
	}
	
	void FixedUpdate ()
	{
		if (Pause.pause)
			return;
		if (Puck.start)
			return;
		if (puck.GetComponent<Rigidbody2D>().position.y > 0) {
			if (lastZone == true)
				timeIn += Time.deltaTime;
			else {
				timeIn = 0;
				lastZone = true;
			}
		} else {
			if (lastZone == false)
				timeOut += Time.deltaTime;
			else {
				timeOut = 0;
				lastZone = false;
			}
		}
		if (counter < 360)
			counter += 0.005f;
		else
			counter = 0;
		
		NewBot ();
		
		//OldBot ();
		
		Vector2 newVelocity = diffSpeed [(int)Settings.difficult] * (position - GetComponent<Rigidbody2D>().position);
		
		//float speed = Vector2.Distance (Vector2.zero, newVelocity);
		
		/*if (!position.Equals (new Vector2 (0, top ? Constants.yStartTop : Constants.yStartBot))) {
			newVelocity = newVelocity / speed * diffSpeed [(int)Settings.difficult];
		}*/
		
		GetComponent<Rigidbody2D>().velocity = newVelocity;
		
	}
	
	void OldBot ()
	{
		if (puck.GetComponent<Rigidbody2D>().position.y > center.y - size.y / 2f && puck.GetComponent<Rigidbody2D>().position.y < center.y + size.y / 2f) {
			if (puck.GetComponent<Rigidbody2D>().position.x < center.x - size.x / 2f + radius || puck.GetComponent<Rigidbody2D>().position.x > center.x + size.x / 2f - radius) {
				position = new Vector2 (0, top ? Constants.yStartTop : Constants.yStartBot);
			} else {
				position = puck.GetComponent<Rigidbody2D>().position;
			}
		} else {
			position = new Vector2 (0, top ? Constants.yStartTop : Constants.yStartBot);
		}
	}
	
	void NewBot ()
	{
		if (puck.GetComponent<Rigidbody2D>().position.x == 0 && puck.position.y >= 0) {
			position = new Vector2 (puck.GetComponent<Rigidbody2D>().position.x + 0.3f, puck.GetComponent<Rigidbody2D>().position.y);
			return;
		}
		if ( (top?puck.GetComponent<Rigidbody2D>().position.y > 7:puck.GetComponent<Rigidbody2D>().position.y < -7) && (puck.GetComponent<Rigidbody2D>().position.x > size.x / 2f - 1f || puck.GetComponent<Rigidbody2D>().position.x < -size.x / 2f + 1f)){
			position = new Vector2 (0, top ? Constants.yStartTop : Constants.yStartBot);
		}
		else {
			if (puck.GetComponent<Rigidbody2D>().position.y > center.y - size.y / 2f && puck.GetComponent<Rigidbody2D>().position.y < center.y + size.y / 2f) {
				position = puck.GetComponent<Rigidbody2D>().position;
			} else {
				if (timeIn > 0.2f && timeOut > 0.2f) {
					if (top) {
						if (timeIn > timeOut) {
							//Debug.Log("def");
							position.y = center.y + 2f + Mathf.Sin (counter);
						} else
							//Debug.Log("attack");
							position.y = center.y - 2f + Mathf.Sin (counter);
						position.x = puck.GetComponent<Rigidbody2D>().position.x - puck.GetComponent<Rigidbody2D>().position.x * radius/4f;
					} else {
						if (timeIn > timeOut) {
							position.y = center.y - 2f + Mathf.Sin (counter);
						} else
							position.y = center.y + 2f + Mathf.Sin (counter);
						position.x = puck.GetComponent<Rigidbody2D>().position.x - puck.GetComponent<Rigidbody2D>().position.x * radius/4f;
					}
				} else {
					//Debug.Log("center");
					position.y = center.y + Mathf.Sin (counter);
					position.x = puck.GetComponent<Rigidbody2D>().position.x - puck.GetComponent<Rigidbody2D>().position.x * radius/4f;
				}
			}
		}
	}
}