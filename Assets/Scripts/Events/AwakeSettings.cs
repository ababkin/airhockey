﻿using UnityEngine;
using System.Collections;

public class AwakeSettings : MonoBehaviour {

	public SpriteRenderer tableSprite, puckSprite, shadowSprite, frameSprite, backgroundSprite;
	public Sprite[] tables, pucks, shadows, frames, backgrounds;
	const float zero = 0.001f;

	void Init ()
	{

	}

	void AcceptSettings ()
	{
		int i = (int)Settings.table;
		tableSprite.sprite = tables [i];
		puckSprite.sprite = pucks [i];
		shadowSprite.sprite = shadows [i];
		frameSprite.sprite = frames [i];
		backgroundSprite.sprite = backgrounds [i];
	}

	void StartInst (Transform obj, float x, float y, float w, float h)
	{
		obj.localPosition = new Vector3 (x, y, 0);
		BoxCollider cr = obj.GetComponent<BoxCollider> ();
		cr.size = new Vector3 (w, h, 1f);
	}

	void Awake ()
	{
		Init ();
		AcceptSettings ();
	}
}