﻿using UnityEngine;
using System.Collections;

public class ResizeManager : MonoBehaviour {
	public GameObject background;

	void Start ()
	{
		Vector3 v = this.transform.position;
		this.transform.position = new Vector3 (v.x, v.y * background.transform.localScale.y, v.z);
	}
}
