using UnityEngine;
using System.Collections;

public class Puck : MonoBehaviour {
	public static float time;
	public static bool start;
	public static float bonusSpeed = 1;
	private Vector2 club;
	void Awake () {
		time = 0;
		start = true;
	}
	void OnCollisionEnter2D (Collision2D coll) {
		start = false;
		if (coll.gameObject.tag == "Club") {
			ContactPoint2D[] cont = coll.contacts;
			club = cont[0].normal;
		}
		if (coll.gameObject.tag == "Untagged") {
			club = Vector2.zero;
		}
	}
	void Update () {
		if (Pause.pause || start) return;
		if (Mathf.Abs (time) > 7) {
			if (time > 0) {
				this.transform.position = new Vector3 (0, Constants.yPuckBot, 0);
				this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				time = 0;
			} else {
				this.transform.position = new Vector3 (0, Constants.yPuckTop, 0);
				this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				time = 0;
			}
		}
		if (this.transform.position.y > Constants.mid) {
			if (time < 0) time = 0;
			else time += Time.deltaTime;
		}
		if (this.transform.position.y < Constants.mid) {
			if (time > 0) time = 0;
			else time -= Time.deltaTime;
		}
	}
	
	void FixedUpdate () {
		float speed = Vector2.Distance (Vector2.zero, GetComponent<Rigidbody2D>().velocity);

		if (GetComponent<Rigidbody2D>().position.x < -5.4f || GetComponent<Rigidbody2D>().position.x > 5.4f)
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (club.x * -0.01f, club.y*0.01f), ForceMode2D.Impulse);
		if (speed > Constants.PUCK_SPEEDS[(int)Settings.speed]) {
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity * (Constants.PUCK_SPEEDS[(int)Settings.speed] * bonusSpeed) / speed;
		}
		
	}
}
