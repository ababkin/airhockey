﻿using UnityEngine;
using System.Collections;

public class HideTextManager : MonoBehaviour {
	const float topHide = 1.8f;
	const float bottomHide = -2.7f;
	public float number = 0;
	TextMesh mesh;

	void Start ()
	{
		mesh = this.GetComponent<TextMesh> ();
		mesh.text += " "+number+" ";
		SetNewPos (this.transform.parent.position.y);
	}

	public void SetNewPos (float y)
	{
		y -= number * Constants.sizeHelpStr;
		if (y < bottomHide)
		{
			mesh.color = new Color (mesh.color.r,mesh.color.g,mesh.color.b,Mathf.Max ((y - bottomHide + 0.3f)/0.3f, 0));
		}
		else
		if (y > topHide)
		{
			mesh.color = new Color (mesh.color.r,mesh.color.g,mesh.color.b,Mathf.Max ((topHide + 0.3f - y)/0.3f, 0));
		}
		else
			mesh.color = new Color (mesh.color.r,mesh.color.g,mesh.color.b,1);
	}
}
