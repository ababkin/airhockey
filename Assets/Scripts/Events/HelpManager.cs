﻿using UnityEngine;
using UnityEngine.UI;

public class HelpManager : MonoBehaviour {
	public Text text;
	void Awake () {
		string str;
		switch (Settings.language) {
		case Language.Russian:
			#region Russian
			str = "Аэрохоккей — это игра с участием двух игроков.\nПравила:";
			str += "\nЗа каждую забитую в ворота противника шайбу присуждается одно очко.";
			str += "Выигрывает тот, кто первым наберет определенное количество очков. ";
			str += "Гол засчитывается, когда шайба падает в ворота.\n";
			str += "Отбивать шайбу игрок может, когда она находится на его половине поля, ";
			str += "причем сделать это он должен в течение 7 секунд. ";
			str += "Если он не отобьет шайбу за это время, шайба достается противнику.";
			#endregion
			break;

		case Language.Spanish:
			#region Spanish
			str = "Hockey de aire es un juego con la participación de dos jugadores.\nLas reglas:";
			str += "\nPor cada tejo clavado en la puerta del enemigo recibirá Usted un punto.";
			str += "Ganan los jugadores que pueden ganar una cierta cantidad de puntos. ";
			str += "El gol está anotado después de que el tejo se mete en la puerta del enemigo.\n";
			str += "El jugador puede rechazar el tejo cuando el está en su parte del ";
			str += "campo y lo tiene que hacer durante de 7 segundos. ";
			str += "Si el jugador no rechazará el tejo durante este período, el tejo se pasará al enemigo.";
			#endregion
			break;

		case Language.French:
			#region French
			str = "Air Hockey est un jeu impliquant deux joueurs.\nLes règles:";
			str += "\nUn point est obtenu pour chaque chaqun but marqué.";
			str += "Les joueurs gagnants peuvent gagner un certain nombre de points.";
			str += "Le but est marqué si la rondelle va dans la porte de l'ennemi.\n";
			str += "Le joueur peut refuser la rondelle quand elle est dans sa partie de terrain pendant 7 secondes. ";
			str += "Si le joueur ne rejette pas la rondelle pendant cette période, la rondelle se déplace l'ennemi.";
			#endregion
			break;

		default:
			#region English
			str = "Airhockey is a two players game.\nRules:";
			str += "\nEach goal brings you one score. ";
			str += "The one who gains certain scores is a winner. ";
			str += "Goal is credited when puck is in the gates.\n";
			str += "Puck may be knocked off when it is on your playing field. ";
			str += "Moreover, a player should do so in 7 seconds otherwise he puck goes to adversary.";
			#endregion
			break;
		}
		text.text = str;
	}
}