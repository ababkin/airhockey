﻿using UnityEngine;
using System.Collections;

public class ShadowBeautiful : MonoBehaviour {
	SpriteRenderer sRenderer;

	void Start () {
		sRenderer = this.GetComponent<SpriteRenderer> ();
	}
	const float SPEED = 1;
	float speed = SPEED;
	void Update () {
		Color color = sRenderer.color;
		if (color.a + speed*Time.deltaTime > 1f) {
			speed = -SPEED;
		} else {
			if (color.a + speed*Time.deltaTime < 0.3f) {
				speed = SPEED;
			} else {
				color.a += speed*Time.deltaTime;
			}
		}
		sRenderer.color = color;
	}
}
