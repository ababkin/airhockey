using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour {
	public Sprite airUp, airDown, notAirUp, notAirDown;

	void Start () {
		if (Settings.table == Settings.Table.Cosmos)
		{
			this.GetComponent<SpriteRenderer> ().sprite = airUp;
			this.GetComponent<Buttons> ().down = airDown;
			this.GetComponent<Buttons> ().up = airUp;
		}
		else
		{
			this.GetComponent<SpriteRenderer> ().sprite = notAirUp;
			this.GetComponent<Buttons> ().down = notAirDown;
			this.GetComponent<Buttons> ().up = notAirUp;
		}
	}
}
