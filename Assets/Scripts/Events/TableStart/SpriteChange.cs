﻿using UnityEngine;
using System.Collections;

public class SpriteChange : MonoBehaviour {
	public Sprite[] sprites;
	
	void Awake () {
		this.GetComponent<SpriteRenderer> ().sprite = sprites [(int)Settings.table];
	}
}