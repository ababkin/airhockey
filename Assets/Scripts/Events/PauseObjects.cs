﻿using UnityEngine;
using System.Collections;

public class PauseObjects : MonoBehaviour {
	Vector2 velocity;
	Vector2 newPos;
	Rigidbody2D body;
	SpriteRenderer sRend;
	bool flag;
	float t;
	void Start () {
		body = this.GetComponent<Rigidbody2D> ();
		flag = false;
		sRend = this.GetComponent<SpriteRenderer> ();
	}
	public void DoPause () {
		if (flag) return;
		flag = true;
		velocity = body.velocity;
		body.velocity = Vector3.zero;
		body.isKinematic = true;
	}
	public void DoContinue () {
		flag = false;
		if (this == null) return; 
		body.isKinematic = false;
//		Debug.Log ("CONTINUE:" + this.gameObject.name + " " +velocity);
		body.velocity = velocity;
	}

	public void ResetPos (Vector2 newPos, bool sPeremes4eniem) {
		this.newPos = newPos;
//		Debug.Log ("RESET: "+this.gameObject.name);
		t = 0;
		if (sPeremes4eniem) {
			Peremeschai ();
		} else {
			if (this == null) return; 
			transform.position = newPos;
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			CoverMouse cMouse = this.GetComponent<CoverMouse> ();
			if (cMouse) {
//				Debug.Log ("reset");
				cMouse.SetPosition (newPos);
			}
			CoverMouseBot cMouseBot = this.GetComponent<CoverMouseBot> ();
			if (cMouseBot) {
//				Debug.Log ("reset bot");
				cMouseBot.SetPosition (newPos);
			}
			sRend.color = Color.white;
		}
	}

	void Peremeschai () {
		t += Time.deltaTime;
		if (t>0.5f) {
			velocity = Vector2.zero;
			if (t<1f) {
				transform.position = newPos;
				sRend.color = new Color (1,1,1,t*2-1);
				Invoke ("Peremeschai", Time.deltaTime);
			} else {
				CoverMouse cMouse = this.GetComponent<CoverMouse> ();
				if (cMouse) {
//					Debug.Log ("reset");
					cMouse.SetPosition (newPos);
				}
				CoverMouseBot cMouseBot = this.GetComponent<CoverMouseBot> ();
				if (cMouseBot) {
//					Debug.Log ("reset bot");
					cMouseBot.SetPosition (newPos);
				}
				sRend.color = Color.white;
			}
		} else {
			sRend.color = new Color (1,1,1,1-t*2);
			Invoke ("Peremeschai", Time.deltaTime);
		}
	}
}