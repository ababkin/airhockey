﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public GameObject exit;
	public GameObject[] other;

    void Start() {
    }

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			bool flag = true;
			if(other.Length > 0){
				foreach (GameObject obj in other) {
					if (obj.activeSelf) {
						flag = false;
						break;
					}
				}
			}

			if (!exit.activeSelf && flag) {
				if (GameData.readyVideo) {
					Debug.Log("video " + GameData.readyVideo);

				} else {
				}
				exit.SetActive (true);
				Buttons.stop = true;
			}
		}
	}
}
